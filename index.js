"use strict"

// 1.Метод setTimeout позволяет вызвать функцию один раз, через определенный промежуток времени,
// в свою очередь setInterval позволяет вызывать функцию регулярно (как цикл), повторяя вызов через
// определенный промежуток времени.
// 2. Функция выполнится сразу после выполнения кода.
// 3. Функцию очистит таймеры, чтобы избежание багов из-за неожиданного срабатывания функций.


const sliderLine = document.querySelector('.slider-line');
const slider = document.querySelector('.slider');
const sliderImages = document.querySelectorAll('.slider-images');
const btnStart = document.querySelector('.button-start');
const btnGroup = document.querySelector('.button-group');
const firstBtn = document.querySelector('.btn-first');
const secondBtn = document.querySelector('.btn-second');

let counter = 0;
let sliderWidth = slider.offsetWidth;

btnStart.addEventListener('click', nextSlide);

function nextSlide () {
    counter++;

    if (counter >= sliderImages.length) {
        counter = 0;
    }

    slideOver();

    let timerId =  setTimeout(() => {
        nextSlide();
    }, 3000);

    firstBtn.addEventListener('click', e => {
        clearTimeout(timerId);

        secondBtn.addEventListener('click', nextSlide);
    });
}

function slideOver () {
    sliderLine.style.transform = `translateX(${-counter * sliderWidth}px)`;
}

function showBnt () {
    for (let item of btnGroup.children) {
        if (item.classList.contains('hidden')) {
            item.classList.remove('hidden');
        }
    }
}
setTimeout(() => {
    showBnt();
}, 6000);